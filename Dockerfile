FROM python:3.7-buster

RUN pip install nose coverage parameterized
RUN apt -y update \
    && apt -y install unzip wget nodejs \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

RUN mkdir /downloads/sonarqube -p \
    && cd /downloads/sonarqube \
    && wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip \
    && unzip sonar-scanner-cli-4.2.0.1873-linux.zip \
    && mv sonar-scanner-4.2.0.1873-linux /opt/sonar-scanner

RUN echo "sonar.host.url=http://sonarqube" > /opt/sonar-scanner/conf/sonar-scanner.properties \
    && echo "sonar.sourceEncoding=UTF-8"